package com.demoapps.architecture.data;

import android.content.Context;
import android.support.annotation.NonNull;

import com.demoapps.architecture.dao.ProductsDao;
import com.demoapps.architecture.dao.ProductsDatabase;
import com.demoapps.architecture.model.Product;
import com.demoapps.architecture.thread.AppExecutors;

import java.util.List;


/**
 * Created by Erdem OLKUN , 23/08/2017
 */

public class LocalProductsDataSource implements ProductsDataSource {

    private AppExecutors appExecutors;

    private ProductsDao productsDao;

    public LocalProductsDataSource(@NonNull Context context) {
        appExecutors = new AppExecutors();
        productsDao = ProductsDatabase.getInstance(context.getApplicationContext()).products();
    }

    @Override
    public void getProducts(final ProductsLoadCallback productsLoadCallback) {
        appExecutors.diskIO().execute(new Runnable() {
            @Override
            public void run() {

                try {
                    final List<Product> products = productsDao.list();
                    if (productsLoadCallback != null) {
                        appExecutors.mainThread().execute(new Runnable() {
                            @Override
                            public void run() {
                                if (products == null || products.size() == 0) {
                                    productsLoadCallback.onError(null);
                                } else {
                                    productsLoadCallback.onProductsLoaded(products);
                                }
                            }
                        });
                    }
                } catch (Exception ex) {
                    if (productsLoadCallback != null) {
                        productsLoadCallback.onError(ex);
                    }
                }

            }
        });

    }

    @Override
    public void saveProducts(@NonNull final List<Product> products, final ProductsUpdateInsertCallback productsUpdateInsertCallback) {

        appExecutors.diskIO().execute(new Runnable() {
            @Override
            public void run() {
                productsDao.deleteAll(); // Clear database cache
                long[] ids = productsDao.insertAll(products);
                appExecutors.mainThread().execute(new Runnable() {
                    @Override
                    public void run() {
                        if (productsUpdateInsertCallback != null) {
                            productsUpdateInsertCallback.onProductsInserted();
                        }
                    }
                });
            }
        });
    }
}
