package com.demoapps.architecture.dao;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.os.Debug;

import com.demoapps.architecture.model.Product;


/**
 * Created by Erdem OLKUN , 23/08/2017
 */

@Database(entities = {Product.class}, version = 1, exportSchema = false)
public abstract class ProductsDatabase extends RoomDatabase {
    /**
     * @return The DAO for the Products table.
     */
    public abstract ProductsDao products();

    /**
     * The only instance
     */
    private static ProductsDatabase sInstance;

    /**
     * Gets the singleton instance of ProductsDatabase.
     *
     * @param context The context.
     * @return The singleton instance of ProductsDatabase.
     */
    public static synchronized ProductsDatabase getInstance(Context context) {
        if (sInstance == null) {
            RoomDatabase.Builder<ProductsDatabase> builder = Room
                    .databaseBuilder(context.getApplicationContext(), ProductsDatabase.class, "products_v1");
            if (Debug.isDebuggerConnected()) {
                builder.allowMainThreadQueries();
            }
            sInstance = builder.build();
        }
        return sInstance;
    }


}
