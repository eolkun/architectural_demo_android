
package com.demoapps.architecture.injection;


import com.demoapps.architecture.core.product.ProductListPresenter;
import com.demoapps.architecture.data.RemoteProductsDataSource;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Erdem OLKUN , 11/07/2017
 */
@Singleton
@Component(modules = {AppModule.class, NetModule.class})
public interface AppComponent {

    void inject(RemoteProductsDataSource remoteProductsDataSource);

    void inject(ProductListPresenter productListPresenter);
}
