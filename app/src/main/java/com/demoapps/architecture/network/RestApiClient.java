
package com.demoapps.architecture.network;

import android.support.annotation.NonNull;

import com.demoapps.architecture.network.response.ProductsResponse;

import io.reactivex.Observable;

/**
 * Created by Erdem OLKUN , 14/03/2017
 */

public class RestApiClient {
    private RestApiService service;


    public RestApiClient(@NonNull RestApiService service) {
        this.service = service;
    }

    public Observable<ProductsResponse> products() {
        return service.products();
    }
}
