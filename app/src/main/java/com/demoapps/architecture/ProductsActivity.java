package com.demoapps.architecture;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;

import com.demoapps.architecture.core.product.ProductListContract;
import com.demoapps.architecture.core.product.ProductListPresenter;
import com.demoapps.architecture.model.Product;
import com.demoapps.architecture.ui.ProductAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindInt;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductsActivity extends AppCompatActivity implements ProductListContract.View {

    @BindView(R.id.rv_products)
    RecyclerView rvProducts;

    @BindView(R.id.v_products_progress_bar)
    View vProgress;


    @BindInt(R.integer.product_span_count)
    int spanCount;

    ProductAdapter productAdapter;

    ProductListPresenter productListPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products);
        ButterKnife.bind(this);

        bindViews();
        productListPresenter = new ProductListPresenter(this);
        ((App) getApplication()).getAppComponent().inject(productListPresenter);

        productListPresenter.fetchProducts();
    }

    private void bindViews() {
        productAdapter = new ProductAdapter(new ArrayList<Product>(), new ProductAdapter.ProductItemListener() {
            @Override
            public void onItemClicked(Product product) {
                Intent productDetailIntent = new Intent(ProductsActivity.this, ProductDetailActivity.class);
                productDetailIntent.putExtra(ProductDetailActivity.EXTRA_PRODUCT, product);
                startActivity(productDetailIntent);
            }
        });
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, spanCount, LinearLayoutManager.VERTICAL, false);
        rvProducts.setLayoutManager(gridLayoutManager);
        rvProducts.setAdapter(productAdapter);
    }

    @Override
    public void onError(Throwable t, String msg) {
        /*
            TODO
            Separate network and service related issues from Throwable instance.(Retrofit HttpException!!)
            Parse service issues and get error codes

        */

        String message = msg != null ? msg : (t != null ? t.getMessage() : "");
        if (TextUtils.isEmpty(message)) {
            message = "Unknown error";// TODO localization;
        }

        new AlertDialog.Builder(this).setTitle("Error").// TODO localization;
                setMessage(message).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        }).show();

    }

    @Override
    public void onShowLoading() {
        vProgress.setVisibility(View.VISIBLE);
    }

    @Override
    public void onDismissLoading() {
        vProgress.setVisibility(View.GONE);
    }

    @Override
    public void onListProducts(List<Product> products) {
        productAdapter.update(products);
    }
}
