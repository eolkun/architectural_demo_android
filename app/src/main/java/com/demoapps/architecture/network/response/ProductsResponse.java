package com.demoapps.architecture.network.response;

import com.demoapps.architecture.model.Product;

import java.util.List;

/**
 * Created by Erdem OLKUN , 22/08/2017
 */

public class ProductsResponse {
    public List<Product> products;
}
