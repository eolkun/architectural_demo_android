
package com.demoapps.architecture.network;


import com.demoapps.architecture.network.response.ProductsResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Headers;

/**
 * Created by Erdem OLKUN , 23/02/2017
 */

public interface RestApiService {

    @Headers({
            "Content-Type: application/json",
            "Accept:application/json"
    })

    @GET("developer-application-test/cart/list")
    Observable<ProductsResponse> products();


}