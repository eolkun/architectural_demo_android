package com.demoapps.architecture.core;

/**
 * Created by @Erdem OLKUN on 24/08/2017.
 */
public interface BaseView<T> {

    void onError(Throwable t, String msg);
}
