package com.demoapps.architecture.ui;

import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.demoapps.architecture.R;
import com.demoapps.architecture.model.Product;
import com.demoapps.architecture.utils.ImageUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Erdem OLKUN , 23/08/2017
 */

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ViewHolder> {

    private
    @Nullable
    List<Product> products;

    private
    @Nullable
    ProductItemListener productItemListener;

    public ProductAdapter(@Nullable List<Product> products, @Nullable ProductItemListener productItemListener) {
        this.products = products;
        this.productItemListener = productItemListener;
    }

    public void update(@Nullable List<Product> products) {
        this.products = products;
        notifyDataSetChanged(); // TODO use diffutil
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.item_product, viewGroup, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (productItemListener!=null){
                    productItemListener.onItemClicked(products.get(holder.getAdapterPosition()));
                }
            }
        });

        Product product = products.get(position);
        holder.tvName.setText(product.name);
        holder.tvPrice.setText(product.price + " TL");

        if (options == null) options = ImageUtils.provideRequestOptions();
        Glide.with(holder.itemView.getContext()).
                load(product.image).
                apply(options).
                into(holder.ivImage);
    }

    private RequestOptions options;

    @Override
    public int getItemCount() {
        return products == null ? 0 : products.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_product_price)
        TextView tvPrice;

        @BindView(R.id.tv_product_name)
        TextView tvName;

        @BindView(R.id.iv_product_image)
        ImageView ivImage;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface ProductItemListener {
        void onItemClicked(Product product);
    }
}
