package com.demoapps.architecture.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;
import android.database.Cursor;

import com.demoapps.architecture.model.Product;

import java.util.List;

/**
 * Created by Erdem OLKUN , 23/08/2017
 * Data access object for Products.
 */
@Dao
public interface ProductsDao {

    /**
     * Inserts a query product into the table.
     *
     * @param product A new products.
     * @return The row ID of the newly inserted product.
     */
    @Insert
    long insert(Product product);

    /**
     * Inserts multiple products into the database
     *
     * @param products An array of new products.
     * @return The row IDs of the newly inserted products.
     */
    @Insert
    long[] insertAll(List<Product> products);


    /**
     * Select all products.
     *
     * @return A {@link Cursor} of all the products in the table.
     */
    @Query("SELECT * FROM " + Product.TABLE_NAME)
    List<Product> list();


    /**
     * Delete a product by the ID.
     *
     * @param id The row ID.
     * @return A number of products deleted. This should always be {@code 1}.
     */
    @Query("DELETE FROM " + Product.TABLE_NAME + " WHERE " + Product.COLUMN_ID + " = :id")
    int deleteById(long id);

    /**
     * Delete a product by the ID.
     *
     * @return A number of products deleted.
     */
    @Query("DELETE  FROM " + Product.TABLE_NAME)
    int deleteAll();

    /**
     * Update the product. The product is identified by the row ID.
     *
     * @param product The product to update.
     * @return A number of products updated. This should always be {@code 1}.
     */
    @Update
    int update(Product product);


}