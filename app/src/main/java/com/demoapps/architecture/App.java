package com.demoapps.architecture;

import android.app.Application;

import com.demoapps.architecture.injection.AppComponent;
import com.demoapps.architecture.injection.AppModule;
import com.demoapps.architecture.injection.DaggerAppComponent;
import com.demoapps.architecture.injection.NetModule;

/**
 * Created by Erdem OLKUN , 22/08/2017
 */

public class App extends Application {
    private static App appInstance;

    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        appInstance = this;
        initAppComponent();
    }

    public static App getInstance() {
        return appInstance;
    }

    private void initAppComponent() {
        appComponent = DaggerAppComponent.builder().
                appModule(new AppModule(this)).
                netModule(new NetModule()).
                build();
    }


    public AppComponent getAppComponent() {
        return appComponent;
    }
}
