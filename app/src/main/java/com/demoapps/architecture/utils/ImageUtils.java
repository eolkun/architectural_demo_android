package com.demoapps.architecture.utils;

import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.demoapps.architecture.R;

/**
 * Created by Erdem OLKUN , 24/08/2017
 */

public class ImageUtils {

    public static RequestOptions provideRequestOptions() {
        return new RequestOptions()
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.ic_launcher)
                .priority(Priority.HIGH);
    }
}
