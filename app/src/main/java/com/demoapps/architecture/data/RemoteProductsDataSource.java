package com.demoapps.architecture.data;

import android.content.Context;
import android.support.annotation.NonNull;

import com.demoapps.architecture.App;
import com.demoapps.architecture.model.Product;
import com.demoapps.architecture.network.RestApiClient;
import com.demoapps.architecture.network.response.ProductsResponse;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;


/**
 * Created by Erdem OLKUN , 23/08/2017
 */

public class RemoteProductsDataSource implements ProductsDataSource {

    @Inject
    RestApiClient apiClient;

    @Inject
    CompositeDisposable compositeDisposable;

    public RemoteProductsDataSource(@NonNull Context context) {
        ((App) context.getApplicationContext()).getAppComponent().inject(this);
    }


    @Override
    public void getProducts(@NonNull final ProductsLoadCallback productsLoadCallback) {
        DisposableObserver<ProductsResponse> disposableObserver = new DisposableObserver<ProductsResponse>() {
            @Override
            public void onNext(@NonNull ProductsResponse productsResponse) {
                productsLoadCallback.onProductsLoaded(productsResponse.products);
            }

            @Override
            public void onError(@NonNull Throwable e) {
                productsLoadCallback.onError(e);
            }

            @Override
            public void onComplete() {

            }
        };
        compositeDisposable.add(apiClient.products().subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).subscribeWith(disposableObserver));

    }

    @Override
    public void saveProducts(@NonNull List<Product> products, final ProductsUpdateInsertCallback productsUpdateInsertCallback) {
        // Skip here. Remote can not save products.
    }
}
