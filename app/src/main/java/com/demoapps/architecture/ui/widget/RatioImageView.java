package com.demoapps.architecture.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;

import com.demoapps.architecture.R;


/**
 * Created by Erdem OLKUN , 23/08/2017
 */
public class RatioImageView extends AppCompatImageView {

    private float imageRatio = 1.0f;
    private boolean ratioByHeight = false;

    public RatioImageView(Context context) {
        super(context);
    }

    public RatioImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public RatioImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs);
    }

    protected void init(Context context, AttributeSet attrs) {
        if (attrs == null) return;
        TypedArray typeArray = context.obtainStyledAttributes(attrs, R.styleable.RatioView);
        imageRatio = typeArray.getFloat(R.styleable.RatioView_ratio, imageRatio);
        ratioByHeight = typeArray.getBoolean(R.styleable.RatioView_ratioByHeight, ratioByHeight);
        typeArray.recycle();
    }

    public void setImageRatio(float imageRatio) {
        this.imageRatio = imageRatio;
        invalidate();
    }

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        try {

            if (!ratioByHeight) {
                int width = MeasureSpec.getSize(widthMeasureSpec);
                int widthWithoutPadding = width - getPaddingLeft() - getPaddingRight();
                int heightCalculated = (int) (widthWithoutPadding / imageRatio) + getPaddingTop() + getPaddingBottom();
                setMeasuredDimension(width, heightCalculated);
            } else {
                int height = MeasureSpec.getSize(heightMeasureSpec);
                int heightWithoutPadding = MeasureSpec.getSize(heightMeasureSpec) - getPaddingTop() - getPaddingBottom();
                int widthCalculated = (int) (heightWithoutPadding * imageRatio) + getPaddingLeft() + getPaddingRight();
                setMeasuredDimension(widthCalculated, height);
            }
        } catch (Exception e) {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }
    }
}