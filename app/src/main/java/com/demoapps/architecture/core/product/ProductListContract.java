package com.demoapps.architecture.core.product;


import com.demoapps.architecture.core.BasePresenter;
import com.demoapps.architecture.core.BaseView;
import com.demoapps.architecture.model.Product;

import java.util.List;

/**
 * Created by Erdem OLKUN , 24/08/2017
 */

public interface ProductListContract {
    interface View extends BaseView<Presenter> {
        void onShowLoading();

        void onDismissLoading();

        void onListProducts(List<Product> products);
    }

    interface Presenter extends BasePresenter {
        void fetchProducts();
    }
}
