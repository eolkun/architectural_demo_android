package com.demoapps.architecture.data;

import android.support.annotation.NonNull;

import com.demoapps.architecture.model.Product;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by Erdem OLKUN , 23/08/2017
 */

public class ProductsRepository implements ProductsDataSource {

    private ProductsDataSource localProductsDataSource;
    private ProductsDataSource remoteProductsDataSource;

    @Inject
    public ProductsRepository(@Named("local") @NonNull ProductsDataSource localProductsDataSource,
                              @Named("remote") @NonNull ProductsDataSource remoteProductsDataSource) {
        this.localProductsDataSource = localProductsDataSource;
        this.remoteProductsDataSource = remoteProductsDataSource;
    }

    @Override
    public void getProducts(final ProductsLoadCallback productsLoadCallback) {
        remoteProductsDataSource.getProducts(new ProductsLoadCallback() {
            @Override
            public void onProductsLoaded(List<Product> products) {
                localProductsDataSource.saveProducts(products, null);
                productsLoadCallback.onProductsLoaded(products);
            }

            @Override
            public void onError(final Throwable tRemote) {
                localProductsDataSource.getProducts(new ProductsLoadCallback() {
                    @Override
                    public void onProductsLoaded(List<Product> products) {
                        productsLoadCallback.onProductsLoaded(products);
                    }

                    @Override
                    public void onError(Throwable t) {
                        productsLoadCallback.onError(tRemote);
                    }
                });
            }
        });

    }

    @Override
    public void saveProducts(@NonNull List<Product> products, ProductsUpdateInsertCallback productsUpdateInsertCallback) {

    }
}
