package com.demoapps.architecture.core;

/**
 * Created by @Erdem OLKUN on 24/08/2017.
 */

public interface BasePresenter {
    void subscribe();

    void unSubscribe();
}
