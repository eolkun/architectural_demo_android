
## Project Bootstrap

* Apply google_code_style.xml file into Intellj Idea code formatting.

## Libraries Used

#### Room

Data persistence library using SqlLite database access. 
Used to cache product list on local storage
https://developer.android.com/topic/libraries/architecture/room.html

#### Butterknife

Dependency injection library for views.
https://github.com/JakeWharton/butterknife

#### Glide

Used for image loading and caching
https://github.com/bumptech/glide

#### Retrofit

Retrofit is an annotation base networking library for android.
I used with RxJava2 observable support.
https://github.com/square/retrofit
 
 #### Dagger2
 
 Used dagger for dependency injection on repository classes and network stuff with rxjava.
 Dependency injections are generated at compile time. 
 
 https://github.com/google/dagger



