package com.demoapps.architecture.core.product;

import android.support.annotation.NonNull;

import com.demoapps.architecture.data.ProductsDataSource;
import com.demoapps.architecture.data.ProductsRepository;
import com.demoapps.architecture.model.Product;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by Erdem OLKUN , 21/02/2017
 */

public class ProductListPresenter implements ProductListContract.Presenter {

    @Inject
    ProductsRepository productsRepository;

    @NonNull
    private ProductListContract.View mView;

    public ProductListPresenter(@NonNull ProductListContract.View view) {
        this.mView = view;
    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unSubscribe() {

    }

    @Override
    public void fetchProducts() {
        mView.onShowLoading();
        productsRepository.getProducts(new ProductsDataSource.ProductsLoadCallback() {
            @Override
            public void onProductsLoaded(List<Product> products) {
                mView.onDismissLoading();
                mView.onListProducts(products);
            }

            @Override
            public void onError(Throwable t) {
                mView.onError(t,null);
                mView.onDismissLoading();
            }
        });
    }
}
