package com.demoapps.architecture;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.demoapps.architecture.model.Product;
import com.demoapps.architecture.utils.ImageUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductDetailActivity extends AppCompatActivity {

    public static final String EXTRA_PRODUCT = "PRODUCT";

    @BindView(R.id.iv_product_image)
    ImageView ivProduct;

    @BindView(R.id.tv_product_name)
    TextView tvName;

    @BindView(R.id.tv_product_price)
    TextView tvPrice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);
        ButterKnife.bind(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //getSupportActionBar().setDisplayShowHomeEnabled(true);

        Product product = (Product) getIntent().getExtras().getSerializable(EXTRA_PRODUCT);

        if (product == null) {
            finish();
            Toast.makeText(this, "No product found", Toast.LENGTH_LONG).show();
            return;
        }
        updateViews(product);

    }

    private void updateViews(Product product) {
        getSupportActionBar().setTitle(product.name);
        Glide.with(this).
                load(product.image).
                apply(ImageUtils.provideRequestOptions()).
                into(ivProduct);

        tvName.setText(product.name);
        tvPrice.setText(product.price + " TL");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }
}
