package com.demoapps.architecture.injection;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import android.app.Application;
import android.support.annotation.NonNull;

import com.demoapps.architecture.injection.qualifiers.ApiUrl;
import com.demoapps.architecture.network.RestApiClient;
import com.demoapps.architecture.network.RestApiService;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Erdem OLKUN , 12/07/2017
 */
@Module
public class NetModule {

    @Provides
    @Singleton
    Gson providesGson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        return gsonBuilder.create();
    }

    @Provides
    Cache provideOkHttpCache(Application application) {
        int cacheSize = 10 * 1024 * 1024; // 10 MiB
        return new Cache(application.getCacheDir(), cacheSize);
    }

    @Provides
    OkHttpClient providesOkHttpClient(Cache cache) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();

        // TODO intercept only on debug builds
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        builder.addInterceptor(interceptor);

        builder.connectTimeout(12, TimeUnit.SECONDS)
                .writeTimeout(12, TimeUnit.SECONDS)
                .readTimeout(12, TimeUnit.SECONDS);
        //builder.cache(cache); // Disable okhttp http disk cache to see local storage disc cache with room library
        return builder.build();
    }


    @Provides
    Retrofit providesRetrofit(@NonNull OkHttpClient client, @NonNull Gson gson, @ApiUrl String url) {
        return new Retrofit.Builder()
                .client(client)
                .baseUrl(url)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson)).build();
    }

    @Provides
    @Singleton
    RestApiClient providesDataApiClient(@NonNull Retrofit retrofit, @NonNull Application application) {
        return new RestApiClient(retrofit.create(RestApiService.class));
    }

    @Provides
    CompositeDisposable providesCompositeDisposable() {
        return new CompositeDisposable();
    }
}
