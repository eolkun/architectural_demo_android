package com.demoapps.architecture.data;

import android.support.annotation.NonNull;

import com.demoapps.architecture.model.Product;

import java.util.List;

/**
 * Created by Erdem OLKUN , 23/08/2017
 */

public interface ProductsDataSource {

    interface ProductsLoadCallback {

        void onProductsLoaded(List<Product> products);

        void onError(Throwable t);
    }

    interface ProductsUpdateInsertCallback {

        void onProductsInserted();
    }

    void getProducts(ProductsLoadCallback productsLoadCallback);

    void saveProducts(@NonNull List<Product> products, ProductsUpdateInsertCallback productsUpdateInsertCallback);

}
