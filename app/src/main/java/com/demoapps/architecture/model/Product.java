package com.demoapps.architecture.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.provider.BaseColumns;

import java.io.Serializable;

/**
 * Created by Erdem OLKUN , 23/08/2017
 */

@Entity(tableName = Product.TABLE_NAME)
public class Product implements Serializable{

    /**
     * The name of the ID column.
     */
    public static final String COLUMN_ID = BaseColumns._ID;


    /**
     * The name of the Products table.
     */
    public static final String TABLE_NAME = "products";

    /**
     * The unique ID of the product.
     */

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(index = true, name = COLUMN_ID)
    public Long id;

    public String product_id;

    @ColumnInfo(name = "name")
    public String name;

    @ColumnInfo(name = "price")
    public Float price;

    @ColumnInfo(name = "image")
    public String image;

}
