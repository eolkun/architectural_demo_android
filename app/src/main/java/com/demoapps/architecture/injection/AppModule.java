package com.demoapps.architecture.injection;

import android.app.Application;
import android.support.annotation.NonNull;

import com.demoapps.architecture.data.LocalProductsDataSource;
import com.demoapps.architecture.data.ProductsDataSource;
import com.demoapps.architecture.data.ProductsRepository;
import com.demoapps.architecture.data.RemoteProductsDataSource;
import com.demoapps.architecture.injection.qualifiers.ApiUrl;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Erdem OLKUN , 11/07/2017
 */
@Module
public class AppModule {
    private final Application application;

    public AppModule(Application application) {
        this.application = application;
    }

    @Provides
    @Singleton
    Application providesApplication() {
        return application;
    }


    @Provides
    @ApiUrl
    String provideBaseUrl() {
        // TODO separate on config files or flavor types for staging and beta builds.
        return "https://s3-eu-west-1.amazonaws.com/";
    }

    @Provides
    @Singleton
    @Named("local")
    ProductsDataSource provideLocalDataSource(@NonNull Application application) {
        return new LocalProductsDataSource(application);
    }

    @Provides
    @Singleton
    @Named("remote")
    ProductsDataSource provideNetworkDataSource(@NonNull Application application) {
        return new RemoteProductsDataSource(application);
    }

    @Singleton
    ProductsRepository provideProductRepository;

}
